package cacheProject.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import cacheProject.service.dto.CountryRS;
import lombok.extern.slf4j.Slf4j;
import cacheProject.repository.CountriesRepository;
import cacheProject.repository.rmodels.Country;

@Service
@Slf4j
public class CountriesService {

   CountriesRepository countriesRepository;

   CacheManager cacheManager;

   @Autowired
   public CountriesService(CountriesRepository countriesRepository, CacheManager cacheManager) {
      this.countriesRepository = countriesRepository;
      this.cacheManager = cacheManager;
   }

   @Cacheable("AllCountries")
   public ResponseEntity<List<CountryRS>> getAllCountries() {
      log.info("Go in to repository");
      List<CountryRS> countryRSList = new ArrayList<>(countriesRepository.findAll().size());
      for (Country country : countriesRepository.findAll()) {
         countryRSList.add(setCountryRS(country));
      }
      return new ResponseEntity<>(countryRSList, HttpStatus.OK);
   }

   @Cacheable("AllNorth")
   public ResponseEntity<List<CountryRS>> getAllNorthCountries() {
      log.info("Go in to repository");
      List<CountryRS> countryRSList = new ArrayList<>(countriesRepository.findAll().size());
      for (Country country : countriesRepository.findAll()) {
         if (country.getLocation().equals("North America")) {
            countryRSList.add(setCountryRS(country));
         }
      }
      return new ResponseEntity<>(countryRSList, HttpStatus.OK);
   }

   @Cacheable("AllCentral")
   public ResponseEntity<List<CountryRS>> getAllCentralCountries() {
      log.info("Go in to repository");
      List<CountryRS> countryRSList = new ArrayList<>(countriesRepository.findAll().size());
      for (Country country : countriesRepository.findAll()) {
         if (country.getLocation().equals("Central America")) {
            countryRSList.add(setCountryRS(country));
         }
      }
      return new ResponseEntity<>(countryRSList, HttpStatus.OK);
   }

   @Cacheable("AllSouth")
   public ResponseEntity<List<CountryRS>> getAllSouthCountries() {
      log.info("Go in to repository");
      List<CountryRS> countryRSList = new ArrayList<>(countriesRepository.findAll().size());
      for (Country country : countriesRepository.findAll()) {
         if (country.getLocation().equals("South America")) {
            countryRSList.add(setCountryRS(country));
         }
      }
      return new ResponseEntity<>(countryRSList, HttpStatus.OK);
   }

   @Cacheable("Country")
   public ResponseEntity<CountryRS> getById(Integer id) {
      log.info("Go in to repository");
      if (countriesRepository.findById(id).isPresent()) {
         return new ResponseEntity<>(setCountryRS(countriesRepository.findById(id).get()), HttpStatus.OK);
      } else {
         CountryRS countryRS = new CountryRS();
         countryRS.setError("Id country not found");
         return new ResponseEntity<>(countryRS, HttpStatus.NOT_FOUND);
      }
   }

   @Scheduled(fixedRate = 6000)
   public void cacheEvict() {
      log.info("Cache clear.");
      evictAllCaches();
   }

   private void evictAllCaches() {
      cacheManager.getCacheNames().forEach(cacheName -> Objects.requireNonNull(cacheManager.getCache("AllCountries")).clear());
      cacheManager.getCacheNames().forEach(cacheName -> Objects.requireNonNull(cacheManager.getCache("AllNorth")).clear());
      cacheManager.getCacheNames().forEach(cacheName -> Objects.requireNonNull(cacheManager.getCache("AllCentral")).clear());
      cacheManager.getCacheNames().forEach(cacheName -> Objects.requireNonNull(cacheManager.getCache("AllSouth")).clear());
      cacheManager.getCacheNames().forEach(cacheName -> Objects.requireNonNull(cacheManager.getCache("Country")).clear());
   }

   private CountryRS setCountryRS(Country country) {
      CountryRS countryRS = new CountryRS();
      countryRS.setId(country.getId());
      countryRS.setName(country.getName());
      countryRS.setLocation(country.getLocation());
      countryRS.setSurface(country.getSurface());
      return countryRS;
   }

}
