package cacheProject.service.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CountryRS {

   private int id;

   private String name;

   private String location;

   private BigDecimal surface;

   private String error;

}
