package cacheProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cacheProject.repository.rmodels.Country;

@Repository
public interface CountriesRepository extends JpaRepository<Country, Integer> {

}
