package cacheProject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import cacheProject.service.CountriesService;
import cacheProject.service.dto.CountryRS;

@RestController
public class CountriesController {

   CountriesService countriesService;

   @Autowired
   public CountriesController(CountriesService countriesService) {
      this.countriesService = countriesService;
   }

   @GetMapping("/countries/all")
   public ResponseEntity<List<CountryRS>> getAllCountries() {
      return countriesService.getAllCountries();
   }

   @GetMapping("/countries/north")
   public ResponseEntity<List<CountryRS>> getAllNorthCountries() {
      return countriesService.getAllNorthCountries();
   }

   @GetMapping("/countries/central")
   public ResponseEntity<List<CountryRS>> getAllCentralCountries() {
      return countriesService.getAllCentralCountries();
   }

   @GetMapping("/countries/south")
   public ResponseEntity<List<CountryRS>> getAllSouthCountries() {
      return countriesService.getAllSouthCountries();
   }

   @GetMapping("/countries/{id}")
   public ResponseEntity<CountryRS> getNorthCountryById(@PathVariable Integer id) {
      return countriesService.getById(id);
   }

}
